//
//  TextFieldWithButton.h
//  textFieldWithButton
//
//  Created by Jose Catala on 13/04/2017.
//  Copyright © 2017 Jose Catala. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TextFieldWithButtonDelegate <NSObject>

@optional

- (void) textFieldButtonAction:(id)sender;

@end

@interface TextFieldWithButton : UITextField <TextFieldWithButtonDelegate>

@property (weak, nonatomic) IBInspectable NSString *btnIdentifier;

@property (weak, nonatomic) id<TextFieldWithButtonDelegate>btnDelegate;

@end
