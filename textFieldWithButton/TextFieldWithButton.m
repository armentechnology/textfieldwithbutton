//
//  TextFieldWithButton.m
//  textFieldWithButton
//
//  Created by Jose Catala on 13/04/2017.
//  Copyright © 2017 Jose Catala. All rights reserved.
//

#import "TextFieldWithButton.h"

@implementation TextFieldWithButton


- (void)drawRect:(CGRect)rect 
{
    // Drawing code
    
    UIButton *rightButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 40, self.frame.size.height)];
    [rightButton addTarget:self action:@selector(rightButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    rightButton.backgroundColor = [UIColor grayColor];
    [rightButton setTitle:@"Add" forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self.rightView = rightButton;
    self.rightViewMode = UITextFieldViewModeAlways;
    
    self.layer.borderWidth = 1;
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

- (IBAction)rightButtonAction:(id)sender
{
    [_btnDelegate textFieldButtonAction:sender];
}

@end
