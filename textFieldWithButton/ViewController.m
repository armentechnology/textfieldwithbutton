//
//  ViewController.m
//  textFieldWithButton
//
//  Created by Jose Catala on 13/04/2017.
//  Copyright © 2017 Jose Catala. All rights reserved.
//

#import "ViewController.h"
#import "TextFieldWithButton.h"

@interface ViewController () <TextFieldWithButtonDelegate>

@property (weak, nonatomic) IBOutlet TextFieldWithButton *txtTest;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _txtTest.btnDelegate = self;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TEXTFIELDBUTTON DELEGATE

- (void) textFieldButtonAction:(id)sender
{
    
    
}

@end
